### Prerequisites:
  + Node (version 8.10+)
  + NPM (tested with 6.5.0)
  + Access to a MongoDB instance (tested using free mLab instance running v4.0.9)
  + Access to scotrail train API

### Instructions:
  0. Clone this repo. 
  1. `npm install --production`
  2. Provide environmental variables:
      + Rename `.env-sample` to `.env` and update the variables
      + Or set the variables from `.env-sample` in your environment using any other method 
  3. `npm run start` to run the app
      + `npm run start` to list trains going from Glasgow to Edinburgh via Bathgate and *persist them to Mongo*
      + `npm run start -- detect` to list trains going from Glasgow to Edinburgh via Bathgate *without saving them*
      + `npm run start -- retrieve` to fetch all trains saved to Mongo

### Dev setup:
  1. `npm install` to add dev dependencies
  2. `npm run build` to compile typescript
  3. `npm run start` to run the app  
  
  To debug, you can add breakpoints to TypeScript source directly and attach debugger to node using `--inspect` flag.

### If scotrail api is down

  1. You're out of luck. I used vagrant to overcome the problem. You could also use docker directly.
  2. Install Vagrant.
  3. Run `vagrant up` from this project root.
  4. Make yourself a cup of tea/coffee
  4. The API should be available at [http://192.168.12.34:8981][0]
  5. You can test by going to [http://192.168.12.34:8981/live/GLQ][1]
  6. When you're done, you can `vagrant destroy`.


[0]: http://192.168.12.34:8981

[1]: http://192.168.12.34:8981/live/GLQ
