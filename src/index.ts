import {Application} from './Application';
import * as dotenv from 'dotenv';

process.on('unhandledRejection', (reason, promise) => {
    console.error(reason);
    throw new Error(promise[Symbol.toStringTag]);
});

dotenv.config();
const app = new Application;

let operationPromise: Promise<void>;
switch (process.argv[2]) {
    case 'detect':
        operationPromise = app.findServices('GLQ', 'Edinburgh', 'Bathgate')
            .then(services => console.log(services));
        break;
    case 'retrieve':
        operationPromise = app.getSaved()
            .then(services => console.log(services));
        break;
    default:
        operationPromise = app.findAndSave()
            .then(services => console.log('Found and saved these services', services));
}

operationPromise
    .then(() => process.exit(0));
