import {Collection, MongoClient} from 'mongodb';
import {TrainRecord} from './TrainRecord';

/**
 * An interface for persisting our trains
 */
export class TrainStorage {

    private mongo: MongoClient;
    private DBNAME: string;
    private COLLECTION: string;

    constructor(connectionString: string, dbName: string, collectionName: string) {
        this.DBNAME = dbName;
        this.COLLECTION = collectionName;
        this.mongo = new MongoClient(connectionString, {useNewUrlParser: true});
        this.shutdownOnExit();
    }

    public async saveService(service: TrainRecord) {
        const collection = await this.getCollection();
        await collection.updateOne(
            {id: service.id},
            {$set: service},
            {upsert: true},
        );
    }

    public async getServices(): Promise<TrainRecord[]> {
        const collection = await this.getCollection();
        return collection.find().toArray();
    }

    private async getCollection(): Promise<Collection<TrainRecord>> {
        const mongo = await this.mongo.connect();
        return mongo.db(this.DBNAME).collection(this.COLLECTION);
    }

    private async shutdownOnExit() {
        const shutdown = () => this.mongo.close();
        process.on('exit', shutdown);
        process.on('SIGINT', shutdown);
        process.on('SIGTERM', shutdown);
        process.on('SIGKILL', shutdown);
        process.on('uncaughtException', shutdown);
    }
}
