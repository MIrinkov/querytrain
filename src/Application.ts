import {TrainApi, TrainService} from './TrainApi';
import {TrainStorage} from './TrainStorage';

export class Application {

    private api: TrainApi;
    private storage: TrainStorage;

    constructor() {
        this.storage = new TrainStorage(
            process.env.MONGO_CONNECTION_STRING!,
            process.env.DB_NAME!,
            process.env.COLLECTION_NAME!);
        this.api = new TrainApi(process.env.TRAIN_API_URL!);
    }

    async findServices(fromCode: string, destinationName: string, viaName: string): Promise<TrainService[]> {
        const trains = await this.api.getStationBoard(fromCode);
        const trainsToDestination = trains.services
            .filter(service => service.destination === destinationName);
        const stops = await Promise.all(
            trainsToDestination.map(service => this.api.getServiceStops(service.id)));

        // zip trains together with stops
        // and filter by whether the stops include `viaName`
        const matchingVia = trainsToDestination
            .map((service, index) => ({service, stops: stops[index]}))
            .filter(result => result.stops.some(stop => stop.Station === viaName));

        return matchingVia.map(x => x.service);
    }

    async findAndSave(): Promise<TrainService[]> {
        const services = await this.findServices('GLQ', 'Edinburgh', 'Bathgate');
        await Promise.all(services.map(s => this.storage.saveService(s)));
        return services;
    }

    async getSaved() {
        return await this.storage.getServices();
    }
}
