import request = require('request-promise-native');

export interface Board {
    station: Station;
    services: TrainService[];
}

export interface Station {
    name: string;
    code: string;
    longitude: number;
    latitude: number;
}

export interface TrainService {
    id: string;
    platform: string;
    destination: string;
    departs: string;
    arrives: string;
    expected: string;
    origin: string;
    operator: string;
}

export interface Stop {
    Station: string;
    Status: string;
    Time: string;
    Connection: Stop[] | null;
}


/**
 * A wrapper around scotrail api
 */
export class TrainApi {

    constructor(private apiURL: string) {}

    getStationBoard(stationCode: string): Promise<Board> {
        const uri = `${this.apiURL}/live/${stationCode}`;
        return request({uri, json: true})
            .promise()
            .catch(() => {
                throw new Error(`Could not fetch live board for station "${stationCode}" from ${uri}`);
            });
    }


    getServiceStops(serviceId: string): Promise<Stop[]> {
        const uri = `${this.apiURL}/service/${serviceId}`;
        return request({uri, json: true})
            .promise()
            .catch(() => {
                throw new Error(`Could not fetch details for service "${serviceId}" from ${uri}`);
            });
    }
}
