"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Application_1 = require("./Application");
const dotenv = __importStar(require("dotenv"));
process.on('unhandledRejection', (reason, promise) => {
    console.error(reason);
    throw new Error(promise[Symbol.toStringTag]);
});
dotenv.config();
const app = new Application_1.Application;
let operationPromise;
switch (process.argv[2]) {
    case 'detect':
        operationPromise = app.findServices('GLQ', 'Edinburgh', 'Bathgate')
            .then(services => console.log(services));
        break;
    case 'retrieve':
        operationPromise = app.getSaved()
            .then(services => console.log(services));
        break;
    default:
        operationPromise = app.findAndSave()
            .then(services => console.log('Found and saved these services', services));
}
operationPromise
    .then(() => process.exit(0));
//# sourceMappingURL=index.js.map