"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const request = require("request-promise-native");
/**
 * A wrapper around scotrail api
 */
class TrainApi {
    constructor(apiURL) {
        this.apiURL = apiURL;
    }
    getStationBoard(stationCode) {
        const uri = `${this.apiURL}/live/${stationCode}`;
        return request({ uri, json: true })
            .promise()
            .catch(() => {
            throw new Error(`Could not fetch live board for station "${stationCode}" from ${uri}`);
        });
    }
    getServiceStops(serviceId) {
        const uri = `${this.apiURL}/service/${serviceId}`;
        return request({ uri, json: true })
            .promise()
            .catch(() => {
            throw new Error(`Could not fetch details for service "${serviceId}" from ${uri}`);
        });
    }
}
exports.TrainApi = TrainApi;
//# sourceMappingURL=TrainApi.js.map