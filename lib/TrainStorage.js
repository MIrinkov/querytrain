"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
/**
 * An interface for persisting our trains
 */
class TrainStorage {
    constructor(connectionString, dbName, collectionName) {
        this.DBNAME = dbName;
        this.COLLECTION = collectionName;
        this.mongo = new mongodb_1.MongoClient(connectionString, { useNewUrlParser: true });
        this.shutdownOnExit();
    }
    saveService(service) {
        return __awaiter(this, void 0, void 0, function* () {
            const collection = yield this.getCollection();
            yield collection.updateOne({ id: service.id }, { $set: service }, { upsert: true });
        });
    }
    getServices() {
        return __awaiter(this, void 0, void 0, function* () {
            const collection = yield this.getCollection();
            return collection.find().toArray();
        });
    }
    getCollection() {
        return __awaiter(this, void 0, void 0, function* () {
            const mongo = yield this.mongo.connect();
            return mongo.db(this.DBNAME).collection(this.COLLECTION);
        });
    }
    shutdownOnExit() {
        return __awaiter(this, void 0, void 0, function* () {
            const shutdown = () => this.mongo.close();
            process.on('exit', shutdown);
            process.on('SIGINT', shutdown);
            process.on('SIGTERM', shutdown);
            process.on('SIGKILL', shutdown);
            process.on('uncaughtException', shutdown);
        });
    }
}
exports.TrainStorage = TrainStorage;
//# sourceMappingURL=TrainStorage.js.map