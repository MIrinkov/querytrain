"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const TrainApi_1 = require("./TrainApi");
const TrainStorage_1 = require("./TrainStorage");
class Application {
    constructor() {
        this.storage = new TrainStorage_1.TrainStorage(process.env.MONGO_CONNECTION_STRING, process.env.DB_NAME, process.env.COLLECTION_NAME);
        this.api = new TrainApi_1.TrainApi(process.env.TRAIN_API_URL);
    }
    findServices(fromCode, destinationName, viaName) {
        return __awaiter(this, void 0, void 0, function* () {
            const trains = yield this.api.getStationBoard(fromCode);
            const trainsToDestination = trains.services
                .filter(service => service.destination === destinationName);
            const stops = yield Promise.all(trainsToDestination.map(service => this.api.getServiceStops(service.id)));
            // zip trains together with stops
            // and filter by whether the stops include `viaName`
            const matchingVia = trainsToDestination
                .map((service, index) => ({ service, stops: stops[index] }))
                .filter(result => result.stops.some(stop => stop.Station === viaName));
            return matchingVia.map(x => x.service);
        });
    }
    findAndSave() {
        return __awaiter(this, void 0, void 0, function* () {
            const services = yield this.findServices('GLQ', 'Edinburgh', 'Bathgate');
            yield Promise.all(services.map(s => this.storage.saveService(s)));
            return services;
        });
    }
    getSaved() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.storage.getServices();
        });
    }
}
exports.Application = Application;
//# sourceMappingURL=Application.js.map